<?php
namespace CMS\ToolsBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of BreadcrumbExtension
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class BreadcrumbExtension extends \Twig_Extension{
    
    private $container = null;
    private $request = null;
    private $router = null;
    private $bundles = null;
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('breadcrumb', [$this, 'breadcrumb'], ['is_safe' => ['html'], 'needs_environment' => true])
        );
    }
    
    public function breadcrumb(\Twig_Environment $environment){
        $breadcrumbs = array();
        
        $currentRouteName = $this->getCurrentRoute();
        $currentRoute = $this->getRoute($currentRouteName);
        
        if (!$currentRoute->hasOption('title')){
            $currentRoute->setOption('title', $this->getBundleName($currentRoute->getDefaults()));
        }
        
        if (null !== $currentRoute->getOption('mapper', null)){
            $route = $this->getRoute($currentRoute->getOption('mapper'));
            if (null !== $route){
                if (!$route->hasOption('title')){
                    $route->setOption('title', $this->getBundleName($route->getDefaults()));
                }

                $breadcrumbs[$currentRoute->getOption('mapper')] = $route;
            }
        }
        
        $breadcrumbs[$currentRouteName] = $currentRoute;
        
        return $environment->render(
                'ToolsBundle:Extension:breadcrumb.html.twig',
                array(
                    'breadcrumbs' => $breadcrumbs
                ));
    }
    
    private function getBundleName(array $defaults){
        // TODO привести к нормальному виду
        $matches = array();
        preg_match('/\\\([a-zA-Z0-9]+)Bundle\\\/', $defaults['_controller'], $matches);
        
        $bundleName = $matches[1] . 'Bundle';
        
        $bundles = $this->getBundles();
        
        if (isset($bundles[$bundleName])){
            if (method_exists($bundles[$bundleName], 'getDiscription')){
                $description = $bundles[$bundleName]->getDiscription();
                
                return $description['title'];
            }
        }
        
        return '';
    }
    
    public function getRoute($name){
        $routerCollection = $this->getRouter()->getRouteCollection();
        $route = $routerCollection->get($name);
        
        if ($route){
            return $route;
        }
        
        return null;
    }
    
    private function getCurrentRoute(){
        return $this->getRequest()->get('_route');
    }
    
    private function getRequest(){
        if (null === $this->request){
            $requestStack = $this->container->get('request_stack');
            $this->request = $requestStack->getCurrentRequest();
        }
        
        return $this->request;
    }
    
    private function getRouter(){
        if (null === $this->router){
            $this->router = $this->container->get('router');
        }
        
        return $this->router;
    }
    
    private function getBundles(){
        if (null === $this->bundles){
            $this->bundles = $this->container->get('kernel')->getBundles();
        }
        
        return $this->bundles;
    }

    public function getName() {
        return 'breadcrumb_extension';
    }
}