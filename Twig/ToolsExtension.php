<?php
namespace CMS\ToolsBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of BreadcrumbExtension
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
class ToolsExtension extends \Twig_Extension{
    
    private $container = null;    
    
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('toString', [$this, 'toString'], ['is_safe' => ['text']])
        );
    }
    
    public function toString($data){
        if (is_bool($data)){
            return ($data === true)? 'true': 'false';
        }
        
        return (string) $data;
    }

    public function getName() {
        return 'tools_extension';
    }
}